## Self Attention GAN
Two kinds of GANs - DCGAN, and a SAGAN.

## Questions to ask
1. Have to submit both a regular DCGAN, and a SAGAN? - Submit DCGAN based on BCE, Wasserstein loss & SAGAN based on Wasserstein, Hinge loss
2. Where to apply attention layer? - After first convolution layer, in both Gen and Dis
3. Should we apply Spectral norm + SA for the WGAN? - Apply SN for only SAGAN
4. Can the WGAN model architecture differ from the DCGAN, and the SAGAN? - Can differ

## Observations
1. Using 32 filter multiplier for WSAGAN is quite beneficial, also attention after the first layer for both G and D, just like how the paper described
2. Added SN to G and D layers in WDCGAN as it reduces blotchiness in generated images - images still aren't as 'defined' as DCGAN

## TODO
3. Also implement conditional GANs - append class labels during forward()
1. Refactor remaining files to have cleaner code
2. Update ipynb files to reflect new code

## Stats
1. DCGAN
    10 epochs - mean fid 279.91 - probably best number of epochs is 10, beyond that FID score just keeps increasing, perhaps limit of DCGAN arch
    30 epochs - mean fid 272.33 - goes back down again after iter 20
    15 epochs - mean fid 114.73 - after batch FID
2. SAGAN
    20 epochs - mean fid 132.788 - after batch FID
    40 epochs - mean fid 133.375 - after batch FID
2. WDCGAN
    50 epochs - mean 285.18
    30 epochs - mean fid 191.141 - after batch FID
3. WSAGAN
    50 epochs - mean 289.90
    30 epochs - mean fid 159.298 - after batch FID
