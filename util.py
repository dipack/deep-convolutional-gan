import os
import shutil
import logging
import torch as t
import torchvision.utils as tutils
import torchvision.datasets as torchdatasets
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
from numpy import argmin
from argparse import ArgumentParser

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)

def setup_argparser():
    parser = ArgumentParser()
    parser.add_argument("--data-path", type=str, help="Path to CIFAR10 dataset - either already downloaded or location to download to", default="./cifar10")
    parser.add_argument("--output-path", type=str, help="Path where we store the output", default="./output")
    parser.add_argument("--num-iterations", type=int, help="Num of iterations to train the GAN for", default=10)
    parser.add_argument("--batch-size", type=int, help="Number of samples in a training batch", default=64)
    parser.add_argument("--latent-size", type=int, help="Number of units in latent representation", default=100)
    parser.add_argument("--gen-units", type=int, help="Multiplier for number of units in generator convolutional units", default=64)
    parser.add_argument("--dis-units", type=int, help="Multiplier for number of units in discriminator convolutional units", default=64)
    parser.add_argument("--log-interval", type=int, help="Log every n iterations", default=100)
    parser.add_argument("--fid-batch-interval", type=int, help="Collect images for FID score calc every n iterations", default=100)
    # parser.add_argument("--learning-rate-dis", type=float, help="Learning rate for Discriminator", default=0.0004)
    # parser.add_argument("--learning-rate-gen", type=float, help="Learning rate for Generator", default=0.0004)
    parser.add_argument("--learning-rate", type=float, help="Learning rate for the GAN", default=0.0002)
    parser.add_argument("--gen-weights", type=str, help="Path to saved weights for Generator", default=None)
    parser.add_argument("--dis-weights", type=str, help="Path to saved weights for Discriminator", default=None)
    parser.add_argument("--critic-iter", type=int, help="Train generator after every n steps the discriminator has spent training", default=5)
    parser.add_argument("--debug", help="Print extra debug info", action="store_true", default=False)
    parser.add_argument("--clean", help="Clean + make output path before run", action="store_true", default=False)
    parser.add_argument("--save-best", help="Save a generated sample of given batch size, from the generator instance with the lowest FID score during training", action="store_true", default=False)
    return parser

def get_dataset(path="./cifar10", train=True):
    dataset = torchdatasets.CIFAR10(root=path, download=True, train=train,
            transform=transforms.Compose([
                # No need for size transforms, as images are already 32*32
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                ]))
    return dataset

def init_layer_weights(layer):
    clsname = layer.__class__.__name__
    if clsname.find('Conv') > -1:
        layer.weight.data.normal_(0.0, 0.02)
    if clsname.find('BatchNorm') > -1:
        layer.weight.data.normal_(1.0, 0.02)
        layer.bias.data.fill_(0)

def plot_losses(gen_losses, dis_losses):
    plt.figure(figsize=(10, 5))
    plt.title("Generator and Discriminator loss during training")
    plt.plot(gen_losses, label="Generator")
    plt.plot(dis_losses, label="Discriminator")
    plt.xlabel("Iterations")
    plt.ylabel("Loss")
    plt.legend()
    plt.show()
    return

def plot_fid_scores(fid_scores):
    plt.figure(figsize=(10, 5))
    plt.title("FID scores over time")
    plt.plot(fid_scores)
    plt.xlabel("Iterations")
    plt.ylabel("FID score")
    plt.show()
    return

def save_best_generator_image(generator, weight_filenames, fid_scores, noise, output_path):
    best_gen_weights = weight_filenames[argmin(fid_scores)]
    logging.info("Generator model with lowest FID score is {}".format(best_gen_weights))
    generator.load_state_dict(t.load(best_gen_weights))
    best_fake_image_path = "{0}/best_fake_sample.png".format(output_path)
    logging.info("Saving output of best generator model to {}".format(best_fake_image_path))
    tutils.save_image(generator(noise).detach(), best_fake_image_path, normalize=True)
    return

def mean_fid(fid_scores):
    return sum(fid_scores) / len(fid_scores)

def clean_make_dir(path):
    if os.path.isdir(path):
        logging.info("Clean selected - removing: {}".format(path))
        shutil.rmtree(path)
    logging.info("Creating output directory: {}".format(path))
    return os.makedirs(path, exist_ok=True)

