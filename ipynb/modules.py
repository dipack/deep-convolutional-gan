import math
import os
import warnings

import numpy as np
import torch as t
import torch.nn as tnn
import torch.nn.functional as F
import torchvision.models as models
from scipy import linalg
from tqdm import tqdm_notebook as tqdm


class SelfAttention(tnn.Module):
    def __init__(self, in_channels):
        super(SelfAttention, self).__init__()
        self.in_channels = in_channels

        self.conv_query = tnn.Conv2d(in_channels=self.in_channels, out_channels=self.in_channels // 8, kernel_size=1)
        self.conv_key = tnn.Conv2d(in_channels=self.in_channels, out_channels=self.in_channels // 8, kernel_size=1)
        self.conv_value = tnn.Conv2d(in_channels=self.in_channels, out_channels=self.in_channels, kernel_size=1)

        self.gamma = tnn.Parameter(t.zeros(1))
        self.softmax = tnn.Softmax(dim=-1)
        self.attention = None
        return

    def forward(self, batch):
        batch_size, channels, cols, rows = batch.size()

        query = self.conv_query(batch).view(batch_size, -1, cols * rows).permute(0, 2, 1)
        key = self.conv_key(batch).view(batch_size, -1, cols * rows)
        value = self.conv_value(batch).view(batch_size, -1, cols * rows)
        energy = t.bmm(query, key)

        attention = self.softmax(energy)

        output = t.bmm(value, attention.permute(0, 2, 1))
        output = output.view(batch_size, channels, cols, rows)
        output = self.gamma * output + batch

        self.attention = attention

        return output

    def get_attention(self, as_image=True):
        if as_image:
            batch_size, rows, cols = self.attention.size()
            return self.attention.view(batch_size, 1, rows, cols)
        return self.attention


# class FID:
#     def __init__(self, device=None):
#         if device is None:
#             device = t.device("cpu")
#         self.device = device
#         # Use pool3 features
#         self.dims = 2048
#         block_idx = InceptionV3.BLOCK_INDEX_BY_DIM[self.dims]
#         self._inception = InceptionV3([block_idx]).to(device=self.device)
#         # Put model into eval mode
#         self._inception.eval()
#
#     def _scale(self, images) -> t.Tensor:
#         scaled = []
#         for img in images:
#             img_scaled = skimage_resize(img, (3, 299, 299))
#             scaled.append(img_scaled)
#         return t.from_numpy(np.asarray(scaled))
#
#     def _predict(self, x: t.Tensor, batch_size):
#         """
#         This function accepts a device to run the calculations on,
#         but,
#         it ALWAYS returns a CPU-bound version of the predictions.
#         I realise this is bad design, but I don't expect to deploy
#         this to prod, so...
#         """
#         if len(x) % batch_size != 0:
#             logging.warn("Number of samples is not a multiple of batch size - some samples will be missed")
#         if len(x) < batch_size:
#             batch_size = len(x)
#
#         num_batches = len(x) // batch_size
#         num_done = num_batches * batch_size
#         preds = np.empty((num_done, self.dims))
#
#         for idx in range(num_batches):
#             start = idx * batch_size
#             end = start + batch_size
#             x_scaled = self._scale(x[start:end].cpu().numpy()).to(device=self.device)
#             batch_size = x_scaled.size(0)
#             x_pred = self._inception(x_scaled)[0]
#             preds[start:end] = x_pred.cpu().data.numpy().reshape(batch_size, -1)
#         return preds
#
#     def _calculate_mu_sigma(self, x_pred):
#         mu = np.mean(x_pred, axis=0)
#         sigma = np.cov(x_pred, rowvar=False)
#         return mu, sigma
#
#     def _calculate_activations(self, x, batch_size):
#         x_pred = self._predict(x, batch_size)
#         return self._calculate_mu_sigma(x_pred)
#
#     def calculate_fid(self, x1: t.Tensor, x2: t.Tensor, batch_size=32):
#         fid = 0.0
#         if x1 is None or x2 is None:
#             return fid
#         mu1, sigma1 = self._calculate_activations(x1.detach(), batch_size)
#         mu2, sigma2 = self._calculate_activations(x2.detach(), batch_size)
#
#         diff = mu1 - mu2
#         cov_mean, _ = sqrt_matrix(np.dot(sigma1, sigma2), disp=False)
#         if not np.isfinite(cov_mean).all():
#             eps = 1e-16
#             offset = np.eye(np.shape(sigma1)[0]) * eps
#             cov_mean = sqrt_matrix(np.dot( (sigma1 + offset), (sigma2 + offset) ))
#         if np.iscomplexobj(cov_mean):
#             cov_mean = cov_mean.real
#         fid = np.dot(diff, diff) + np.trace(sigma1) + np.trace(sigma2) - 2 * np.trace(cov_mean)
#         return fid
#

class FID():
    def __init__(self, cache_dir="~/.cache", device="cpu", transform_input=True):
        # cuda:0
        os.environ["TORCH_HOME"] = cache_dir
        self.device = device
        self.transform_input = transform_input
        self.InceptionV3 = models.inception_v3(pretrained=True, transform_input=False, aux_logits=False).to(device=self.device)
        self.InceptionV3.eval()

    def build_maps(self, x):
        # Resize to Fit InceptionV3
        if list(x.shape[-2:]) != [299, 299]:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                x = F.interpolate(x, size=[299, 299], mode='bilinear')
        # Transform Input to InceptionV3 Standards
        if self.transform_input:
            x_ch0 = t.unsqueeze(x[:, 0], 1) * (0.229 / 0.5) + (0.485 - 0.5) / 0.5
            x_ch1 = t.unsqueeze(x[:, 1], 1) * (0.224 / 0.5) + (0.456 - 0.5) / 0.5
            x_ch2 = t.unsqueeze(x[:, 2], 1) * (0.225 / 0.5) + (0.406 - 0.5) / 0.5
            x = t.cat((x_ch0, x_ch1, x_ch2), 1)
        # Run Through Partial InceptionV3 Model
        with t.no_grad():
            # N x 3 x 299 x 299
            x = self.InceptionV3.Conv2d_1a_3x3(x)
            # N x 32 x 149 x 149
            x = self.InceptionV3.Conv2d_2a_3x3(x)
            # N x 32 x 147 x 147
            x = self.InceptionV3.Conv2d_2b_3x3(x)
            # N x 64 x 147 x 147
            x = F.max_pool2d(x, kernel_size=3, stride=2)
            # N x 64 x 73 x 73
            x = self.InceptionV3.Conv2d_3b_1x1(x)
            # N x 80 x 73 x 73
            x = self.InceptionV3.Conv2d_4a_3x3(x)
            # N x 192 x 71 x 71
            x = F.max_pool2d(x, kernel_size=3, stride=2)
            # N x 192 x 35 x 35
            x = self.InceptionV3.Mixed_5b(x)
            # N x 256 x 35 x 35
            x = self.InceptionV3.Mixed_5c(x)
            # N x 288 x 35 x 35
            x = self.InceptionV3.Mixed_5d(x)
            # N x 288 x 35 x 35
            x = self.InceptionV3.Mixed_6a(x)
            # N x 768 x 17 x 17
            x = self.InceptionV3.Mixed_6b(x)
            # N x 768 x 17 x 17
            x = self.InceptionV3.Mixed_6c(x)
            # N x 768 x 17 x 17
            x = self.InceptionV3.Mixed_6d(x)
            # N x 768 x 17 x 17
            x = self.InceptionV3.Mixed_6e(x)
            # N x 768 x 17 x 17
            x = self.InceptionV3.Mixed_7a(x)
            # N x 1280 x 8 x 8
            x = self.InceptionV3.Mixed_7b(x)
            # N x 2048 x 8 x 8
            x = self.InceptionV3.Mixed_7c(x)
            # N x 2048 x 8 x 8
            # Adaptive average pooling
            x = F.adaptive_avg_pool2d(x, (1, 1))
            # N x 2048 x 1 x 1
            return x

    def calculate_fid(self, real_images, generated_images, batch_size=64):
        # Ensure Set Sizes are the Same
        assert (real_images.shape[0] == generated_images.shape[0])
        # Build Random Sampling Orders
        real_images = real_images[np.random.permutation(real_images.shape[0])]
        generated_images = generated_images[np.random.permutation(generated_images.shape[0])]
        # Lists of Maps per Batch
        real_maps = []
        generated_maps = []
        # Build Maps
        for s in range(math.ceil(real_images.shape[0] / batch_size)):
            sidx = np.arange(batch_size * s, min(batch_size * (s + 1), real_images.shape[0]))
            real_maps.append(self.build_maps(real_images[sidx].to(device=self.device)).detach().to(device='cpu'))
            generated_maps.append(
                self.build_maps(generated_images[sidx].to(device=self.device)).detach().to(device='cpu'))
        # Concatenate Maps
        real_maps = np.squeeze(t.cat(real_maps).numpy())
        generated_maps = np.squeeze(t.cat(generated_maps).numpy())
        # Calculate FID
        # Activation Statistics
        mu_g = np.mean(generated_maps, axis=0)
        mu_x = np.mean(real_maps, axis=0)
        sigma_g = np.cov(generated_maps, rowvar=False)
        sigma_x = np.cov(real_maps, rowvar=False)
        # Sum of Squared Differences
        ssd = np.sum((mu_g - mu_x) ** 2)
        # Square Root of Product of Covariances
        covmean = linalg.sqrtm(sigma_g.dot(sigma_x), disp=False)[0]
        if np.iscomplexobj(covmean):
            covmean = covmean.real
        # Final FID Computation
        return ssd + np.trace(sigma_g + sigma_x - 2 * covmean)
