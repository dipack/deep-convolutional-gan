import os
import shutil
import logging
import torch as t
import torch.nn as tnn
import torch.nn.functional as F
import torch.optim as toptimizer
import torch.backends.cudnn as tcudnn
import torchvision.utils as tutils
from modules import SelfAttention, FID
from util import setup_argparser, get_dataset, init_layer_weights, plot_losses, plot_fid_scores, clean_make_dir, save_best_generator_image, mean_fid

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)

class WSAGenerator(tnn.Module):
    def __init__(self, latent_size=100, gen_hidden_units=64):
        super(WSAGenerator, self).__init__()
        self.attn_layer = SelfAttention(gen_hidden_units * 8)
        self.l1 = tnn.Sequential(
            # Input layer
            tnn.utils.spectral_norm(tnn.ConvTranspose2d(latent_size, out_channels=gen_hidden_units * 8, kernel_size=4, stride=1, padding=0, bias=False)),
            tnn.BatchNorm2d(gen_hidden_units * 8),
            tnn.ReLU(True)
                )
        self.l2 = tnn.Sequential(
            # Layer 2
            tnn.utils.spectral_norm(tnn.ConvTranspose2d(gen_hidden_units * 8, gen_hidden_units * 4, 4, 2, 1, bias=False)),
            tnn.BatchNorm2d(gen_hidden_units * 4),
            tnn.ReLU(True)
                )
        self.l3 = tnn.Sequential(
            # Layer 3
            tnn.utils.spectral_norm(tnn.ConvTranspose2d(gen_hidden_units * 4, gen_hidden_units * 2, 4, 2, 1, bias=False)),
            tnn.BatchNorm2d(gen_hidden_units * 2),
            tnn.ReLU(True)
                )
        self.l4 = tnn.Sequential(
            # Layer 4
            tnn.utils.spectral_norm(tnn.ConvTranspose2d(gen_hidden_units * 2, gen_hidden_units, 4, 2, 1, bias=False)),
            tnn.BatchNorm2d(gen_hidden_units),
            tnn.ReLU(True)
                )
        self.lout = tnn.Sequential(
            # Output Layer
            # CIFAR-10 reps RGB images, so there are 3 channels in the output
            # Changes made to the final layer to work with 32 * 32
            tnn.ConvTranspose2d(gen_hidden_units, out_channels=3, kernel_size=1, stride=1, padding=0, bias=False),
            tnn.Tanh()
                )
        return

    def forward(self, x):
        output = self.l1(x)
        output = self.attn_layer(output)
        output = self.l2(output)
        output = self.l3(output)
        output = self.l4(output)
        output = self.lout(output)
        return output

class WSADiscriminator(tnn.Module):
    def __init__(self, latent_size=100, dis_hidden_units=64):
        super(WSADiscriminator, self).__init__()
        self.num_hidden_units = dis_hidden_units
        self.attn_layer = SelfAttention(dis_hidden_units)
        self.l1 = tnn.Sequential(
                # Input layer
                tnn.utils.spectral_norm(tnn.Conv2d(in_channels=3, out_channels=dis_hidden_units, kernel_size=4, stride=2, padding=1, bias=False)),
                tnn.LeakyReLU(0.2, inplace=True)
                )
        self.l2 = tnn.Sequential(
                # Layer 1
                tnn.utils.spectral_norm(tnn.Conv2d(dis_hidden_units, dis_hidden_units * 2, 4, 2, 1, bias=False)),
                tnn.BatchNorm2d(dis_hidden_units * 2),
                tnn.LeakyReLU(0.2, inplace=True)
                )
        self.l3 = tnn.Sequential(
                # Layer 2
                tnn.utils.spectral_norm(tnn.Conv2d(dis_hidden_units * 2, dis_hidden_units * 4, 4, 2, 1, bias=False)),
                tnn.BatchNorm2d(dis_hidden_units * 4),
                tnn.LeakyReLU(0.2, inplace=True),
                )
        self.l4 = tnn.Sequential(
                # Layer 3
                tnn.utils.spectral_norm(tnn.Conv2d(dis_hidden_units * 4, dis_hidden_units * 8, 4, 2, 1, bias=False)),
                tnn.BatchNorm2d(dis_hidden_units * 8),
                tnn.LeakyReLU(0.2, inplace=True),
                )
        self.lout = tnn.Sequential(
                # Output Layer
                tnn.Linear(4 * 4 * 4 * dis_hidden_units, 1, bias=False)
                )
        return

    def forward(self, x):
        output = self.l1(x)
        output = self.attn_layer(output)
        output = self.l2(output)
        output = self.l3(output)
        output = self.l4(output)
        output = output.view(-1, 4 * 4 * 4 * self.num_hidden_units)
        output = self.lout(output)
        return output

def main():
    args = setup_argparser().parse_args()

    path             = args.data_path
    output_path      = args.output_path
    learning_rate    = args.learning_rate
    batch_size       = args.batch_size
    latent_size      = args.latent_size
    num_epochs       = args.num_iterations
    gen_hidden_units = args.gen_units
    dis_hidden_units = args.dis_units
    debug_info       = args.debug
    clean_dirs       = args.clean
    critic_iters     = args.critic_iter
    save_best        = args.save_best
    log_interval     = args.log_interval
    fid_batch_interval = args.fid_batch_interval

    if clean_dirs:
        clean_make_dir(output_path)

    logging.info("Dataset path: {}, Output path: {}, Batch Size: {}, Latent Size: {}, Num iterations: {}, Generator unit multiplier: {}, Discriminator unit multiplier: {}, Learning rate: {}".format(path, output_path, batch_size, latent_size, num_epochs, gen_hidden_units, dis_hidden_units, learning_rate))

    training_dataset = get_dataset(path, train=True)
    # Dropping the last batch as it is sometimes not divisible by batch_size,
    # causing errors when trying to feed it to the GAN.
    training_data_loader = t.utils.data.DataLoader(training_dataset, batch_size=batch_size, shuffle=True, drop_last=True)
    testing_dataset_path = "{0}/test".format(path)
    testing_dataset = get_dataset(testing_dataset_path, train=False)
    testing_data_loader = t.utils.data.DataLoader(testing_dataset, batch_size=batch_size, shuffle=True, drop_last=True)
    testing_data_iter = iter(testing_data_loader)

    device = t.device("cpu")
    if t.cuda.is_available():
        logging.info("CUDA enabled device found - will use CUDA device")
        device = t.device("cuda:0")

    # Init FID calculator
    fid_calculator = FID()

    gen_weights = args.gen_weights
    generator = WSAGenerator(latent_size=latent_size, gen_hidden_units=gen_hidden_units).to(device)
    generator.apply(init_layer_weights)
    if gen_weights is not None:
        logging.info("Using weights saved at {} to continue Generator training".format(gen_weights))
        generator.load_state_dict(t.load(gen_weights))

    dis_weights = args.dis_weights
    discriminator = WSADiscriminator(latent_size=latent_size, dis_hidden_units=dis_hidden_units).to(device)
    discriminator.apply(init_layer_weights)
    if dis_weights is not None:
        logging.info("Using weights saved at {} to continue Discriminator training".format(dis_weights))
        discriminator.load_state_dict(t.load(dis_weights))

    logging.info(generator)
    logging.info(discriminator)

    noise = t.randn(batch_size, latent_size, 1, 1, device=device)

    dis_optimizer = toptimizer.RMSprop(discriminator.parameters(), lr=learning_rate)
    gen_optimizer = toptimizer.RMSprop(generator.parameters(), lr=learning_rate)

    gen_filenames = []
    gen_losses = []
    dis_losses = []
    fid_scores = []

    # START - Training epoch loop
    for epoch in range(num_epochs):
        # Variables to hold generated and fake images for FID score calc
        real_image_batch, fake_image_batch = None, None
        # START - Training batch loop
        for idx, data in enumerate(training_data_loader, start=0):
            # DISCRIMINATOR
            # Train discriminator network
            discriminator.zero_grad()
            # Real data
            real_images = data[0].to(device)
            # Batch size
            bs = real_images.size(0)

            output = discriminator(real_images)
            # Hinge loss
            dis_err_real = output.mean()
            # Discriminator output mean for real images
            D_x = output.mean().item()

            # Train with fake data
            epoch_noise = t.randn(batch_size, latent_size, 1, 1, device=device)
            # Generate fake images using noise
            fake_images = generator(epoch_noise)
            # Discriminator output for fake images
            output = discriminator(fake_images.detach())
            # Hinge loss
            dis_err_fake = output.mean()
            # Discriminator output mean for fake images - before discriminator update
            D_G_z1 = output.mean().item()
            dis_err = -(dis_err_real - dis_err_fake)
            discriminator.zero_grad()
            dis_err.backward()
            dis_optimizer.step()

            for param in discriminator.parameters():
                param.data.clamp_(-0.01, 0.01)

            if idx % critic_iters == 0:
                # GENERATOR
                # Train with fake data
                epoch_noise = t.randn(batch_size, latent_size, 1, 1, device=device)
                fake_images = generator(epoch_noise)
                # Test discriminator again - this time after training it
                output = discriminator(fake_images)
                # Hinge loss
                gen_err = -(output.mean())
                generator.zero_grad()
                gen_err.backward()
                # Discriminator output mean for fake images - after discriminator update
                D_G_z2 = output.mean().item()
                gen_optimizer.step()

                gen_losses.append(gen_err.item())
                dis_losses.append(dis_err.item())

            if idx % log_interval == 0:
                log_info = "Epoch: {}/{}, Sample: {}/{}, Disc Loss: {:.4f}, Gen Loss: {:.4f}".format(epoch + 1, num_epochs, idx + 1, len(training_data_loader), dis_err.item(), gen_err.item())
                if debug_info:
                    log_info += ", D(x): {:.4f}, D(G(z)): {:.4f} / {:.4f}".format(D_x, D_G_z1, D_G_z2)
                logging.info(log_info)

            if idx % 100 == 0 or idx == (len(training_data_loader) - 1):
                tutils.save_image(real_images, "{0}/real_samples.png".format(output_path), normalize=True)
                fake_images = generator(noise)
                tutils.save_image(fake_images.detach(), "{0}/fake_samples_epoch_{1}.png".format(output_path, epoch + 1), normalize=True)

            if idx % fid_batch_interval == 0:
                # Add to fake image batch for FID score calc
                if fake_image_batch is None:
                    fake_image_batch = fake_images.detach()
                else:
                    fake_image_batch = t.cat((fake_image_batch, fake_images.detach()), dim=0)
        # END - Training batch loop

        g_weight_filename = "{0}/generator_epoch_{1}.pth".format(output_path, epoch + 1)
        gen_filenames.append(g_weight_filename)
        # Checkpoint model
        t.save(generator.state_dict(), g_weight_filename)
        t.save(discriminator.state_dict(), "{0}/discriminator_epoch_{1}.pth".format(output_path, epoch + 1))

        # Save attention map
        tutils.save_image(discriminator.attn_layer.get_attention(as_image=True), "{0}/discriminator_attention_epoch_{1}.png".format(output_path, epoch), normalize=True)
        tutils.save_image(generator.attn_layer.get_attention(as_image=True), "{0}/generator_attention_epoch_{1}.png".format(output_path, epoch), normalize=True)

        # Calculate FID scores after each epoch - as it is super slow during normal iterations
        while real_image_batch is None or len(real_image_batch) < len(fake_image_batch):
            try:
                r_im, _ = next(testing_data_iter)
                r_im.to(device=device)
                if real_image_batch is None:
                    real_image_batch = r_im
                else:
                    real_image_batch = t.cat((real_image_batch, r_im), dim=0)
            except StopIteration:
                testing_data_iter = iter(testing_data_loader)
        fid_score = fid_calculator.calculate_fid(real_image_batch, fake_image_batch, batch_size)
        fid_scores.append(fid_score)
        logging.info("Epoch {} FID score: {}".format(epoch + 1, fid_score))
    # END - Training epoch loop

    logging.info("FID scores over the epochs: {}".format(fid_scores))
    logging.info("Mean FID score: {}".format(mean_fid(fid_scores)))
    plot_losses(gen_losses, dis_losses)
    plot_fid_scores(fid_scores)

    if save_best:
        save_best_generator_image(generator, gen_filenames, fid_scores, noise, output_path)
    return

if __name__ == "__main__":
    main()
